/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import tr.com.cs.atr2prov.AtrMapper;
import tr.com.cs.atr2prov.Discoverer;
import tr.com.cs.atr2prov.ProviderLoader;

/**
 *
 * @author Ferhat
 */
public class TestMain {
  public static void main(String[] args) throws Exception {
    System.out.println(Discoverer.getDiscoverer().pkcs11ConfigForFirstCard());    
    
    ProviderLoader.getInstance().load();
    
    System.err.println(ProviderLoader.getInstance().getAlias());
    System.err.println(ProviderLoader.getInstance().getCert().getSubjectDN().getName());    
  }
}
