/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.atr2prov;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.ATR;
import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactory;
import javax.swing.JOptionPane;

/**
 *
 * @author Ferhat
 */
public class Discoverer {

  private static Discoverer discoverer = null;

  private Discoverer() {
  }

  public static synchronized Discoverer getDiscoverer() {
    if (discoverer == null) {
      discoverer = new Discoverer();
    }

    return discoverer;
  }

  private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

  private static String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }

  public Map<String, String> pkcs11ConfigForFirstCard() throws Exception {
    TerminalFactory factory = TerminalFactory.getDefault();
    Logger.getLogger(Discoverer.class.getName()).log(Level.FINE, "Discovering smart card readers (card terminals).");
    List<CardTerminal> terminalList = factory.terminals().list(CardTerminals.State.ALL);

    if (terminalList == null || (terminalList != null && terminalList.size() == 0)) {
      throw new Exception("No smart card readers (card terminals)!");
    }

    for (int i = 0; i < terminalList.size(); i++) {
      CardTerminal cardTerminal = terminalList.get(i);

      if (!cardTerminal.isCardPresent()) {
        continue;
      }

      try {
        Card card = cardTerminal.connect("*");
        ATR atr = card.getATR();
        card.getBasicChannel().getChannelNumber();
//        try {
//          Thread.sleep(2000);
//        } catch (InterruptedException ie) {
//        }
        card.disconnect(false);
        Logger.getLogger(Discoverer.class.getName()).log(Level.INFO, "Card ATR " + bytesToHex(atr.getBytes()));
        if (AtrMapper.getMapper().cardName(bytesToHex(atr.getBytes())) == null ||
                AtrMapper.getMapper().libraryName(bytesToHex(atr.getBytes())) == null) {
          JOptionPane.showMessageDialog(null, "İmza cihazınız tanıdığımız cihazlardan değil.\n" +
                  "\n" +
                  "Lütfen bu hatanın bir görüntüsünü alıp (Alt+PrtScr ile panoya kopyalayabilirsiniz)\n" +
                  "destek isteğinize ekleyiniz.\n" +
                  "\n" +
                  "Ayrıca, biliyorsanız imza cihazınızın ve akıllı kartın üreticisini,\n" +
                  "bilmiyorsanız sertifika hizmet sağlayıcınızın adını destek isteğinize ekleyiniz.\n" +
                  "\n" +
                  "Tanımlanmamış kart, ATR " + bytesToHex(atr.getBytes()),
                  "Hata!", JOptionPane.ERROR_MESSAGE);
          throw new Exception("ATR " + bytesToHex(atr.getBytes()) + " not known!");
        }
        Map<String, String> configMap = new HashMap<>();
        configMap.put("name", "eisvi-" + AtrMapper.getMapper().cardName(bytesToHex(atr.getBytes())));
        configMap.
                put("library", 
                        "C:/Windows/System32/" + 
                                AtrMapper.getMapper().libraryName(bytesToHex(atr.getBytes())) + ".dll");
        configMap.put("slotListIndex", "0");
        configMap.put("showInfo", "true");
        return configMap;

      } catch (CardException t) {
        Logger.getLogger(Discoverer.class.getName()).log(Level.WARNING, "Discovering smart card reader " + cardTerminal.
                getName(), t);
      }
    }

    return null;
  }
}
