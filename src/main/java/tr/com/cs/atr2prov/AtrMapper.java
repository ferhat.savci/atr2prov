/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.atr2prov;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Ferhat
 */
public class AtrMapper {

  private Map<String, String> atr2Card = new HashMap<>();
  private Map<String, String> card2Lib = new HashMap<>();

  private static AtrMapper mapper = null;

  private AtrMapper() throws Exception {
    InputStream is = AtrMapper.class.getResourceAsStream("/atrlist.xml");
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(is);
    Element root = doc.getDocumentElement();
    NodeList nodes = root.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      Node n = nodes.item(i);
      if (n.getNodeType() != Node.ELEMENT_NODE) {
        continue;
      }

      if ("card-type".equalsIgnoreCase(n.getLocalName())) {
        Element elm = (Element) n;
        String card = elm.getAttribute("name");

        {
          NodeList libNl = elm.getElementsByTagName("lib");
          if (libNl.getLength() != 1) {
            throw new RuntimeException("atr2prov configuration >>fubar<< for " + card + " : " + libNl.getLength() + " libs!");
          }

          Element libElm = (Element) libNl.item(0);
          String libName = libElm.getAttribute("name");

          card2Lib.put(card, libName);
        }
        
        {
          NodeList atrNl = elm.getElementsByTagName("atr");
          if (atrNl.getLength() < 1) {
            throw new RuntimeException("atr2prov configuration >>fubar<< for " + card + " : no ATRs!");
          }

          for (int t = 0; t < atrNl.getLength(); t++) {
            Element atrElm = (Element) atrNl.item(t);
            String atrVal = atrElm.getAttribute("value");
            atr2Card.put(atrVal, card);
          }
        }
      }
    }

    Logger.getLogger(AtrMapper.class.getName()).log(Level.CONFIG, "ATR -> Card : " + atr2Card.toString());
    Logger.getLogger(AtrMapper.class.getName()).log(Level.CONFIG, "Card -> Lib : " + card2Lib.toString());
  }

  public static synchronized AtrMapper getMapper() throws Exception {
    if (mapper == null) {
      mapper = new AtrMapper();
    }

    return mapper;
  }

  public String cardName(String atr) {
    return atr2Card.get(atr);
  }
  
  public String libraryName(String atr) {
    String card = atr2Card.get(atr);

    if (card != null) {
      return card2Lib.get(card);
    }

    return null;
  }
}
