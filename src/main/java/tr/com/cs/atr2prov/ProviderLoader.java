/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tr.com.cs.atr2prov;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.pkcs11.wrapper.CK_ATTRIBUTE;
import sun.security.pkcs11.wrapper.PKCS11;
import sun.security.pkcs11.wrapper.PKCS11Constants;

/**
 *
 * @author Ferhat
 */
public class ProviderLoader {
  
  private static ProviderLoader loader = new ProviderLoader();
  private ProviderLoader() {    
  }
  
  public static ProviderLoader getInstance() {
    return loader;
  }
  
  private String providerName;
  private String alias;
  private X509Certificate cert;
  private Map<String, String> pkcs11Config;

    public Map<String, String> getPkcs11Config() {
        return pkcs11Config;
    }

  public String getProviderName() {
    return providerName;
  }

  public String getAlias() {
    return alias;
  }

  public X509Certificate getCert() {
    return cert;
  }

  public void load() throws Exception {
    pkcs11Config = Discoverer.getDiscoverer().pkcs11ConfigForFirstCard();
    
    providerName = "SunPKCS11-"+pkcs11Config.get("name");

    String configStr = "";

    for (Map.Entry<String, String> e : pkcs11Config.entrySet()) {
      configStr = configStr + e.getKey() + "=" + e.getValue() + "\n";
    }

    ByteArrayInputStream bais = new ByteArrayInputStream(configStr.getBytes());
    Class cls = Class.forName("sun.security.pkcs11.SunPKCS11");
    Constructor ctor = cls.getConstructor(InputStream.class);
    Object prvdr = ctor.newInstance(bais);
    Provider sunPKCS11 = (Provider) prvdr;
    Security.addProvider(sunPKCS11);

    PKCS11 p11w = PKCS11.getInstance(pkcs11Config.get("library"), "C_GetFunctionList", null, false);
    long[] slots = p11w.C_GetSlotList(true);
    long session = p11w.C_OpenSession(slots[0], PKCS11Constants.CKF_SERIAL_SESSION, null, null);
    List<String> aliases = new ArrayList<>();
    CK_ATTRIBUTE[] fattrs = new CK_ATTRIBUTE[1];
    fattrs[0] = new CK_ATTRIBUTE();

    fattrs[0].type = PKCS11Constants.CKA_CLASS;
    fattrs[0].pValue = PKCS11Constants.CKO_CERTIFICATE;

    p11w.C_FindObjectsInit(session, fattrs);
    long[] list = p11w.C_FindObjects(session, 1);
    p11w.C_FindObjectsFinal(session);

    if (list.length > 0) {
      char[] chars = null;
      CK_ATTRIBUTE[] attrs = new CK_ATTRIBUTE[]{new CK_ATTRIBUTE(PKCS11Constants.CKA_LABEL)};

      p11w.C_GetAttributeValue(session, list[0], attrs);

      if (attrs[0].pValue != null) {
        chars = attrs[0].getCharArray();
      }

      alias = new String(chars);

      attrs = new CK_ATTRIBUTE[]{new CK_ATTRIBUTE(PKCS11Constants.CKA_VALUE)};
      p11w.C_GetAttributeValue(session, list[0], attrs);

      byte[] bytes = attrs[0].getByteArray();
      if (bytes == null) {
        throw new CertificateException("unexpectedly retrieved null byte array for " + new String(chars));
      }
      CertificateFactory cf = CertificateFactory.getInstance("X.509");
      cert = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(bytes));

      Logger.getLogger(ProviderLoader.class.getName()).log(Level.CONFIG, cert.toString());
    }

    Logger.getLogger(ProviderLoader.class.getName()).log(Level.CONFIG, alias);
  }
}
